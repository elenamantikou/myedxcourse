import { Component } from '@angular/core';
import { CognitiveService } from "../common/services/cognitive.service";
import { ImageResult } from "../common/models/bingSearchResponse";
import { ComputerVisionResponse, ComputerVisionRequest } from '../common/models/computerVisionResponse';

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})

export class SearchComponent {
    searchResults: ImageResult[] | null;
    isSearching = false;
    currentAnalytics: ComputerVisionResponse | null;
    currentItem: ImageResult | null;
    isAnalyzing = false;
//numberOfPeople = 0;

    constructor(private cognitiveService: CognitiveService) { }

    search(searchTerm: string) {
        console.log("search.component::search:: searchTerm=" + searchTerm);
        this.searchResults = null;
        this.currentAnalytics = null;
        this.isSearching = true;
        this.cognitiveService.searchImages(searchTerm)
            .subscribe(result => {
                this.searchResults = result.value;
                this.isSearching = false;
                //this.numberOfPeople  = result.value.filter()
            });
    }

    analyze(result: ImageResult) {
        this.currentItem = result;
        this.currentAnalytics = null;
        this.isAnalyzing = true;
        this.cognitiveService.analyzeImage({ url: result.thumbnailUrl } as ComputerVisionRequest)
            .subscribe(result => {
                this.currentAnalytics = result;
                this.isAnalyzing = false;
            });
        window.scroll(0, 0);
    }
}
