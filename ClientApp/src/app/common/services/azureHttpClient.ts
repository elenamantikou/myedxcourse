import { Injectable } from '@angular/core';
//import { Http, Headers } from "@angular/http";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BingSearchResponse } from '../models/bingSearchResponse';
import { ComputerVisionResponse } from '../models/computerVisionResponse';

@Injectable()
export class AzureHttpClient {
    constructor(private http: HttpClient) { }
    get(url: string, apiKey: string): Observable<BingSearchResponse> {
        console.log("azureHttpClient::get:: url=" + url);
        let headers = new HttpHeaders({ 'Ocp-Apim-Subscription-Key': apiKey });
        return this.http.get<BingSearchResponse>(url, {
            headers: headers
        });
    }
    post(url, apiKey, data): Observable<ComputerVisionResponse> {
        console.log("azureHttpClient::post:: url=" + url);
        //console.log("azureHttpClient::post:: data=" + data.url);
        let headers = new HttpHeaders({ 'Ocp-Apim-Subscription-Key': apiKey });
        let result = this.http.post<ComputerVisionResponse>(url, data, {
            headers: headers
        });
        console.log(result);
        return result;
    }
}