import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { catchError } from 'rxjs/operators';
import { AzureHttpClient } from './azureHttpClient';
import { BingSearchResponse } from '../models/bingSearchResponse';
import { ComputerVisionRequest, ComputerVisionResponse } from "../models/computerVisionResponse";

@Injectable()
export class CognitiveService {
    bingSearchAPIKey = 'cd326acf32fb466ba64e2a4ca802fa3a';
    computerVisionAPIKey = '1658103a59cd44ff9aae628a9851406a';
    constructor(private http: AzureHttpClient) { }
    searchImages(searchTerm: string): Observable<BingSearchResponse> {
        console.log("cognitive.service::searchImages:: searchTerm=" + searchTerm);
        return this.http.get('https://api.cognitive.microsoft.com/bing/v7.0/images/search?q=' + searchTerm, this.bingSearchAPIKey)
            .pipe(catchError(this.handleError));
    }

    analyzeImage(request: ComputerVisionRequest): Observable<ComputerVisionResponse> {
        return this.http.post('https://northeurope.api.cognitive.microsoft.com/vision/v1.0/analyze?visualFeatures=Description,Tags,Faces', this.computerVisionAPIKey, request)
            //.map(response => response.json() as ComputerVisionResponse)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: any): Promise<any> {
        console.error('handleError::An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}