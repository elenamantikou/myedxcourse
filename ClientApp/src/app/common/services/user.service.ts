import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

//import 'rxjs/Rx';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/operator/map';

import { User, AADUser } from '../models/user';

@Injectable()
export class UserService {
    private originUrl: string;
    private aadUser: AADUser;

    constructor(private http: HttpClient, @Inject('BASE_URL') originUrl: string) {
        console.log(originUrl);
        this.originUrl = originUrl;
    }

    public getUser(): Observable<User> {
        return this.http.get(this.originUrl + '/.auth/me')
            .pipe(map(response => {
                try {
                    console.log('response[0]=', response[0]);
                    this.aadUser = response[0] as AADUser;
                    console.log('this.aadUser=', this.aadUser);
                    let user = new User();
                    user.userId = this.aadUser.user_id;

                    this.aadUser.user_claims.forEach(claim => {
                        switch (claim.typ) {
                            case "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname":
                                user.firstName = claim.val;
                                break;
                            case "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname":
                                user.lastName = claim.val;
                                break;
                                case "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/jobtitle":
                                user.jobTitle = claim.val;
                                break;
                        }
                    });

                    // user.firstName = 'My first name';
                    // user.lastName = 'My last name';

                    return user;
                }
                catch (Exception) {
                    console.log('Error: ${Exception}');
                }
            }),
                catchError(this.handleError));
    }
   
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
